# kunkka

#### 介绍
Like Octotree for gitee.com


```

> 代码清晰, 简单, 不超过100行代码, 详见(src/kunkka/ccontent.js).
> 当然功能也很简单, 就是有个浏览树, 发现access_token可以不用, 所以就没实现. :(
> 感谢Octotree, 感谢jQuery, 感谢gitee.
> API看这: https://gitee.com/api/v5/swagger#/getV5ReposOwnerRepoStargazers?ex=no
> 怎么用? chrome F12的扩展程序, 加入src/kunkka就行了.

```

![avatar](t0.png)


#### 说明

```

昆卡，铁帆海军的舰队统帅，在洪流恶魔现身于人类的土地时，担负起守卫他祖国诸岛的使命。

```