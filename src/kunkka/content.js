
/**
 *  Author: Luoyh(Roy)
 *  Date: April 27, 2019
 */
(() => {
    $(function() {
        var _timestamp = +new Date();
        var _id = '__token_' + _timestamp;
        var _li = '__li_' + _timestamp;
        var _in  = true;
        var _template = '\
        <div style="height:100%;width:320px;background:#ccc;position:fixed;left:0;top:0;overflow:auto;">   \
            <input id="'+_id+'" style="width:80%;height:30px;margin:50px auto 10px 20px;" placeholder=" Not support"/>    \
        ';
        // a bug when location end with '#'
        var nice   = window.location.href.match(/^https:\/\/gitee.com\/([^/]+)\/([^/]+)(\/(tree|blob)\/([^/]+))?(\/(.*))?/);
        var owner  = nice[1];
        var repo   = nice[2];
        var branch = nice && nice[5] || 'master'; // default master
        var sha    = branch;
        var type   = nice[4] || '';
        var path   = nice[7] || '';
        var token  = $('#'+_id).val()||''; // may be null, not support token
        if (!owner || !repo || !branch) {
            // oh, wtf? 
            return;
        }

        var intoFolder = (sha, dom, path) => {
            if (!_in) return;
            $.ajax({
                url: 'https://gitee.com/api/v5/repos/'+owner+'/'+repo+'/git/trees/'+sha+(token?('?access_token='+token):''),
                data: {},
                type: 'GET',
                async: false,
                success: r => {
                    var blob = [], tree = [];
                    if (r && r.tree && r.tree.length > 0) {
                        blob = r.tree.filter(e => e.type == 'blob');
                        tree = r.tree.filter(e => e.type == 'tree');
                        var sort = (a, b) => a.path.toLocaleLowerCase().charCodeAt(0) - b.path.toLocaleLowerCase().charCodeAt(0);
                        blob&&blob.length>0&&blob.sort(sort);
                        tree&&tree.length>0&&tree.sort(sort);
                        var view = '<ul>';
                        if (tree.length > 0) {
                            tree.forEach((e, _) => {
                                view += '<li><a href="javascript:;" class="'+_li+'" data-type="tree" data-tree="'+e.path+'" data-path="'+path+'/'+e.path+'" data-hash="'+e.sha+'" style="font-size:20px;">+</a><span>'+e.path+'</span></li>'
                            });
                        }
                        if (blob.length > 0) {
                            blob.forEach((e, _) => {
                                view += '<li style="cursor: pointer;" data-type="blob" class="'+_li+'" data-path="'+path+'/'+e.path+'"><u>'+e.path+'</u></li>';
                            });
                        }
        
                        view += '</ul>';
                        if (dom === 'body') {
                            $('body').append(_template + view + '</div>');
                        } else {
                            $(dom).text('=>').removeClass(_li).closest('li').append(view);
                        }
                        
                    }
                },
                error: () => {
                    _in = false;
                },
                dataType: 'json'
              });
        };

        intoFolder(sha, 'body', '');
        $(document).on('click', '.' + _li, function () {
            var that = $(this);
            if (that.data('type') == 'tree') {
                intoFolder(that.data('hash'), this, that.data('path'));
            } else if (that.data('type') == 'blob') {
                window.location.href='/'+owner+'/'+repo+'/blob/'+branch+that.data('path');
            }
        });
        
        if ((type && type == 'blob') && path) {
            var father = null;
            var ps = decodeURI(path).split('/');
            if (ps && ps.length > 1) { // into folder
                ps.forEach((val, _) => {
                    if (_ == ps.length - 1) { // last the blob, change color
                        father.closest('li').find('ul .' + _li).each((i, e) => {
                            if ($(e).data('type') == 'blob' && $(e).text() == val) {
                                $(e).find('u').css('color', 'red');
                            }
                        });
                        return;
                    }
                    $('.' + _li).each((i, e) => {
                        if ($(e).data('type') == 'tree' && $(e).text() == '+' && $(e).data('tree') == val) {
                            $(e).click();
                            father = $(e);
                            return false;
                        }
                    });
                });
            }
        }
        
    });
})();